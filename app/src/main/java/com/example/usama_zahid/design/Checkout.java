package com.example.usama_zahid.design;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.example.usama_zahid.design.MainActivity.ROOT_URL;

public class Checkout extends AppCompatActivity {

    SharedPreferences userPref;
    private  String userid;
    TextView priceTextView;
    EditText name,email,phone,address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userPref = getSharedPreferences("userdata", MODE_PRIVATE);
        userid = userPref.getString("userid", null);
        if (userid == null) {
            finish();
            Toast.makeText(getApplicationContext(),"Please Login First", Toast.LENGTH_LONG).show();
            startActivity(new Intent(this, Login.class));
            return;
        }
        userRequest(ROOT_URL+"customer-json.php?id="+userid);
        totalRequest(ROOT_URL+"order-json.php?cid="+userid+"&gettotal=yes");

        setContentView(R.layout.activity_checkout);

        LinearLayout ordernow = findViewById(R.id.submitorder);
        ordernow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderRequest(ROOT_URL+"order-json.php?cid="+userid+"&total="+priceTextView.getText());
            }
        });


    }
    public void totalRequest(String url){

        StringRequest totalRequest = new StringRequest(Request.Method.GET,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
               // Toast.makeText(getApplicationContext(),"Yes response is run "+response,Toast.LENGTH_LONG).show();
                try {
                    JSONArray totalJsonArray = new JSONArray(response);
                    for(int i=0;i<totalJsonArray.length();i++){
                        JSONObject checktotal = totalJsonArray.getJSONObject(i);
                        double total = checktotal.getDouble("total");
                        priceTextView = findViewById(R.id.totalbill);
                        priceTextView.setText(""+total);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Toast.makeText(getApplicationContext(),"Yes response is run "+response,Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Network Error No Internet",Toast.LENGTH_LONG).show();
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(totalRequest);

    }

    public void orderRequest(String url){
        StringRequest orderRequest = new StringRequest(Request.Method.GET,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                        if(response.equals("good")){
                            showCustomDialog();
                           // Toast.makeText(getApplicationContext(),"Order Placed Successfully",Toast.LENGTH_LONG).show();

                        }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Network Error No Internet"+error,Toast.LENGTH_LONG).show();
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(orderRequest);

    }

    public void userRequest(String url){

        StringRequest userRequest = new StringRequest(Request.Method.GET,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    //Toast.makeText(getApplicationContext(),"Yes response is run "+response,Toast.LENGTH_LONG).show();
                    JSONArray userJsonArray = new JSONArray(response);
                    for(int i=0;i<userJsonArray.length();i++){
                        JSONObject product = userJsonArray.getJSONObject(i);

                        String data_name = product.getString("name");
                        String data_email = product.getString("email");
                        String data_phone = product.getString("phone");
                        String data_address = product.getString("address");

                        name = findViewById(R.id.checkout_name);
                        email = findViewById(R.id.checkout_email);
                        phone = findViewById(R.id.checkout_phone);
                        address = findViewById(R.id.checkout_address);

                        name.setText(data_name);
                        email.setText(data_email);
                        phone.setText(data_phone);
                        address.setText(data_address);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Toast.makeText(getApplicationContext(),"Yes response is run "+response,Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Network Error No Internet",Toast.LENGTH_LONG).show();
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(userRequest);

    }

    private void showCustomDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_order_placed);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        ((AppCompatButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent gotohome = new Intent(getApplicationContext(),MainActivity.class);
                gotohome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(gotohome);
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
}
