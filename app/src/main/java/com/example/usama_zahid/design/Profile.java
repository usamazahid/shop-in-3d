package com.example.usama_zahid.design;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.example.usama_zahid.design.MainActivity.ROOT_URL;

public class Profile extends AppCompatActivity {


    NestedScrollView container;
    LinearLayout lyt_progress;

    SharedPreferences userPref;
    private  String userid;
    private final String CUSTOMER_URL = ROOT_URL+"customer-json.php";
    TextView name,email,phone,address,head_name,head_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        userPref = getSharedPreferences("userdata", MODE_PRIVATE);
        userid = userPref.getString("userid", null);
        if (userid == null) {
            finish();
            Toast.makeText(getApplicationContext(),"Please Login First", Toast.LENGTH_LONG).show();
            startActivity(new Intent(this, Login.class));
            return;
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        container = findViewById(R.id.profile_content);
        lyt_progress = findViewById(R.id.lyt_progress);

        container.setVisibility(View.INVISIBLE);
        lyt_progress.setVisibility(View.VISIBLE);

        userRequest(CUSTOMER_URL+"?id="+userid);

        LinearLayout logout = findViewById(R.id.logout);
        LinearLayout cart = findViewById(R.id.cart);
        LinearLayout aboutapp = findViewById(R.id.aboutapp);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userPref.edit().remove("userid").commit();
                finish();
                Toast.makeText(getApplicationContext(),"Account Logged Out Successfully ",Toast.LENGTH_SHORT).show();
                Intent gotohome = new Intent(getApplicationContext(),MainActivity.class);
                gotohome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(gotohome);
            }
        });

        aboutapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),About.class));
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { startActivity(new Intent(getApplicationContext(),Cart.class)); }
        });

    }//oncreate end here



    public void userRequest(String url){

        StringRequest userRequest = new StringRequest(Request.Method.GET,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    //Toast.makeText(getApplicationContext(),"Yes response is run "+response,Toast.LENGTH_LONG).show();
                    JSONArray userJsonArray = new JSONArray(response);
                    for(int i=0;i<userJsonArray.length();i++){
                        JSONObject product = userJsonArray.getJSONObject(i);
                        String data_name = product.getString("name");
                        String data_email = product.getString("email");
                        String data_phone = product.getString("phone");
                        String data_address = product.getString("address");

                        name = findViewById(R.id.user_name);
                        email = findViewById(R.id.user_email);
                        phone = findViewById(R.id.user_phone);
                        address = findViewById(R.id.user_address);
                        head_name = findViewById(R.id.head_username);
                        head_email = findViewById(R.id.head_email);

                        name.setText(data_name);
                        email.setText(data_email);
                        head_name.setText("Welcome  "+data_name);
                        head_email.setText("email : "+data_email);
                        phone.setText(data_phone);
                        address.setText(data_address);
                    }

                    lyt_progress.setVisibility(View.GONE);
                    lyt_progress.setAlpha(1.0f);
                    container.setVisibility(View.VISIBLE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Toast.makeText(getApplicationContext(),"Yes response is run "+response,Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Network Error No Internet",Toast.LENGTH_LONG).show();
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(userRequest);

    }

}
