package com.example.usama_zahid.design.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.usama_zahid.design.ProductDetail;
import com.example.usama_zahid.design.Products;
import com.example.usama_zahid.design.R;

import java.util.ArrayList;

public class Category_List_Adapter extends RecyclerView.Adapter<Category_List_Adapter.ViewHolder> {

    Context context;
    ArrayList<CategoryItems> cat_list;

    public Category_List_Adapter(Context context, ArrayList<CategoryItems> cat_list) {
        this.context = context;
        this.cat_list = cat_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.category,parent,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final CategoryItems categoryItems = cat_list.get(position);
        holder.title.setText(categoryItems.getTitle());
        Glide.with(context).load(categoryItems.getImage()).into(holder.imageView);
        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent products = new Intent(context, Products.class);
                products.putExtra("category_id",categoryItems.getId());
                context.startActivity(products);
                //Toast.makeText(context,""+categoryItems.getId(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return cat_list.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView title;
        LinearLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = itemView.findViewById(R.id.cat_imageView);
            this.title = itemView.findViewById(R.id.cat_title);
            parentLayout = itemView.findViewById(R.id.category_item);
        }
    }

}
