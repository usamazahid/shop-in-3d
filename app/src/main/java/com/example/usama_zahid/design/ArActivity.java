package com.example.usama_zahid.design;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.PixelCopy;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.ar.core.Anchor;
import com.google.ar.core.Session;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.ArSceneView;
import com.google.ar.sceneform.assets.RenderableSource;
import com.google.ar.sceneform.rendering.Light;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.TransformableNode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.example.usama_zahid.design.MainActivity.PRODUCTS_URL;
import static com.example.usama_zahid.design.MainActivity.ROOT_URL;

public class ArActivity extends AppCompatActivity {

    RelativeLayout view;
    TextView textView;
    private ArFragment arFragment;
    LinearLayout lyt_progress;
    FloatingActionButton capture;

    private String MODEL_LINK ="";
    int loaderstatus = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent in = getIntent();
        int product_id = in.getIntExtra("product_id",0);

        setContentView(R.layout.activity_ar);

        //getting model  from model field in database here
        lyt_progress = findViewById(R.id.lyt_progress);
        textView = findViewById(R.id.whitetext);
        getModelNameFromServer(product_id);

        arFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.ux_fragment);

        arFragment.getArSceneView().getScene().addOnUpdateListener(
                frameTime -> {arFragment.onUpdate(frameTime);}
        );

        arFragment.setOnTapArPlaneListener((hitResult, plane, motionEvent) -> {
            placeModel(hitResult.createAnchor());
            textView.setVisibility(View.GONE);
            if(loaderstatus == 0){
                lyt_progress.setVisibility(View.VISIBLE);
                loaderstatus = 1;
            }
           // Toast.makeText(this,"Please Wait while downloading the model",Toast.LENGTH_LONG).show();
        });
//        screen capture
        capture = findViewById(R.id.screenshot);
        capture.setOnClickListener(view->takePhoto());
    }

    private void getModelNameFromServer(int id) {
        String url = PRODUCTS_URL+"?id="+id;
        StringRequest productsRequest = new StringRequest(Request.Method.GET,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    String model_field = "";
                    String modelName = "";
                    JSONArray productsJsonArray = new JSONArray(response);
                    for(int i=0;i<productsJsonArray.length();i++){
                        JSONObject product = productsJsonArray.getJSONObject(i);
                        int pid = product.getInt("id");
                        model_field = product.getString("model_3d");
                    }
                    String[] modelarray = model_field.split(",");
                    for(int i=0;i<modelarray.length;i++){
                        if(modelarray[i].contains(".gltf")){
                            modelName = ROOT_URL+"images/3D-Models/"+id+"/"+modelarray[i];
                            MODEL_LINK = modelName.trim();
                            //Toast.makeText(getApplicationContext(),"MODEL LINK NOW :\n"+MODEL_LINK,Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Toast.makeText(getApplicationContext(),"Yes response is run "+response,Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Response Error"+error,Toast.LENGTH_LONG).show();
            }
        });


        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(productsRequest);
    }

    private void placeModel(Anchor anchor) {
        ModelRenderable
                .builder()
                .setSource(
                        getApplicationContext(),
                        RenderableSource
                                .builder()
                                .setSource(getApplicationContext(), Uri.parse(MODEL_LINK),RenderableSource.SourceType.GLTF2)
                                .setRecenterMode(RenderableSource.RecenterMode.ROOT)
                                .build()
                )
                .setRegistryId(MODEL_LINK)
                .build()
                .thenAccept(modelRenderable -> addNodeToScene(modelRenderable,anchor))
                .exceptionally(
                        throwable -> {
                            Toast.makeText(this, "Error Fetching model from server \n"+throwable, Toast.LENGTH_LONG).show();
                            return null;
                        }
                );

    }

    private void addNodeToScene(ModelRenderable modelRenderable, Anchor anchor) {

        AnchorNode anchorNode = new AnchorNode(anchor);
        anchorNode.setParent(arFragment.getArSceneView().getScene());

       // anchorNode.getScene().onRemoveChild(anchorNode.getParent());
       // anchorNode.setLocalPosition(0f,0f,-1f);
        //for disable shadow
        //  arFragment.getArSceneView().getScene().getSunlight().setEnabled(false);

        // Create the transformable model and add it to the anchor.
        TransformableNode transformableNode = new TransformableNode(arFragment.getTransformationSystem());
        transformableNode.setOnTapListener((hitTestResult, motionEvent) -> {
            transformableNode.onDeactivate();
        });

        transformableNode.getScaleController().setMaxScale(0.5f);
        transformableNode.getScaleController().setMinScale(0.3f);


        transformableNode.setParent(anchorNode);
        transformableNode.setRenderable(modelRenderable);
        transformableNode.select();

        lyt_progress.setVisibility(View.GONE);
    }

//=============================================Picture Capture Code====================================================
    //pictures saved in Pictures/Sceneform/ here
    private String generateFilename() {
        String date = new SimpleDateFormat("yyyyMMddHHmmss", java.util.Locale.getDefault()).format(new Date());
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + File.separator + "Sceneform/" + date + "_screenshot.jpg";
    }
    private void saveBitmapToDisk(Bitmap bitmap, String filename) throws IOException {

        File out = new File(filename);
        if (!out.getParentFile().exists()) {
            out.getParentFile().mkdirs();
        }
        try (FileOutputStream outputStream = new FileOutputStream(filename);
             ByteArrayOutputStream outputData = new ByteArrayOutputStream()) {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputData);
            outputData.writeTo(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException ex) {
            throw new IOException("Failed to save bitmap to disk", ex);
        }
    }

    private void takePhoto() {
        Toast.makeText(getApplicationContext(),"Saving...",Toast.LENGTH_LONG).show();
        final String filename = generateFilename();
        ArSceneView view = arFragment.getArSceneView();

        // Create a bitmap the size of the scene view.
        final Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),
                Bitmap.Config.ARGB_8888);

        // Create a handler thread to offload the processing of the image.
        final HandlerThread handlerThread = new HandlerThread("PixelCopier");
        handlerThread.start();
        // Make the request to copy.
        PixelCopy.request(view, bitmap, (copyResult) -> {
            if (copyResult == PixelCopy.SUCCESS) {
                try {
                    saveBitmapToDisk(bitmap, filename);
                } catch (IOException e) {
                    Toast toast = Toast.makeText(ArActivity.this, e.toString(),
                            Toast.LENGTH_LONG);
                    toast.show();
                    return;
                }
                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                        "Photo saved", Snackbar.LENGTH_LONG);
                snackbar.setAction("Open in Photos", v -> {
                    File photoFile = new File(filename);

                    Uri photoURI = FileProvider.getUriForFile(ArActivity.this,
                            ArActivity.this.getPackageName() + ".ar.codelab.name.provider",
                            photoFile);
                    Intent intent = new Intent(Intent.ACTION_VIEW, photoURI);
                    intent.setDataAndType(photoURI, "image/*");
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(intent);

                });
                snackbar.show();
            } else {
                Toast toast = Toast.makeText(ArActivity.this,
                        "Failed to copyPixels: " + copyResult, Toast.LENGTH_LONG);
                toast.show();
            }
            handlerThread.quitSafely();
        }, new Handler(handlerThread.getLooper()));
    }



}

