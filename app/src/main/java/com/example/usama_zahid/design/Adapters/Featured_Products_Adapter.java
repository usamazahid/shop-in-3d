package com.example.usama_zahid.design.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.usama_zahid.design.MainActivity;
import com.example.usama_zahid.design.Models.Product_List_Model;
import com.example.usama_zahid.design.ProductDetail;
import com.example.usama_zahid.design.R;

import java.util.ArrayList;

public class Featured_Products_Adapter extends RecyclerView.Adapter<Featured_Products_Adapter.ViewHolder>{

    Context context;
    ArrayList<Product_List_Model> arr;

    public Featured_Products_Adapter(Context products, ArrayList<Product_List_Model> arr) {
        this.context = products;
        this.arr = arr;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v= inflater.inflate(R.layout.feature_product_box,parent,false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final Product_List_Model model  = arr.get(position);
        //holder.product_image.setImageResource(model.getProduct_image());
        holder.product_title.setText(model.getProduct_title());
        holder.product_des.setText(model.getProduct_des());
        holder.product_price.setText(model.getProduct_price());

        //Toast.makeText(context,""+model.getProduct_image(),Toast.LENGTH_SHORT).show();
        //Glide.with(context).load(model.getProduct_image()).thumbnail(0.1f).into(holder.product_image);
        Glide.with(context).load(model.getProduct_image()).thumbnail(0.1f).into(holder.product_image);

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent productdetial = new Intent(context, ProductDetail.class);
                productdetial.putExtra("product_id",model.getId());
                context.startActivity(productdetial);
                //Toast.makeText(context,""+model.getId(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arr.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView product_image;
        TextView product_title,product_des,product_price;
        LinearLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            product_image = itemView.findViewById(R.id.image_3);
            product_title = itemView.findViewById(R.id.product_title);
            product_des = itemView.findViewById(R.id.product_des);
            product_price = itemView.findViewById(R.id.product_price);
            parentLayout = itemView.findViewById(R.id.feature_product_box);

        }
    }
}
