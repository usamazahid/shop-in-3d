package com.example.usama_zahid.design.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.usama_zahid.design.MainActivity;
import com.example.usama_zahid.design.Models.CartModel;
import com.example.usama_zahid.design.Models.Product_List_Model;
import com.example.usama_zahid.design.ProductDetail;
import com.example.usama_zahid.design.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.usama_zahid.design.MainActivity.ROOT_URL;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartHolder> {
    Context context;
    ArrayList<CartModel> items;
    String userid;

    public CartAdapter(Context context, ArrayList<CartModel> items,String userid) {
        this.context = context;
        this.items = items;
        this.userid = userid;
    }
    @NonNull
    @Override
    public CartHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(context);
        View v=inflater.inflate(R.layout.cart,parent,false);
        CartHolder holder = new CartHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull CartHolder holder, int position) {
        CartModel index = items.get(position);

        holder.title.setText(index.getProduct_title());
        holder.desc.setText(index.getProduct_des());
        holder.price.setText(index.getProduct_price());
        holder.qty.setText(index.getQty()+"");
        Glide.with(context).load(index.getProduct_image()).thumbnail(0.1f).into(holder.image);

        //increment in cart
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringRequest cartRequest = new StringRequest(Request.Method.POST,ROOT_URL+"cart-json.php", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject cartdata = new JSONObject(response);
                            String result = (String) cartdata.get("message");
                            if(result.equals("success")){
                                Toast.makeText(context,"Cart Updated Successfully",Toast.LENGTH_SHORT).show();
                                int qty = index.getQty()+1;
                                holder.qty.setText(""+qty);
                                notifyDataSetChanged();
                            }
                            else if(result.equals("fail")){
                                Toast.makeText(context,"Item not Added ",Toast.LENGTH_SHORT).show();
                            }
                        }
                        catch (JSONException e){
                            e.printStackTrace();
                            Toast.makeText(context,"Faild to get response"+e,Toast.LENGTH_SHORT).show();

                        }
                        //Toast.makeText(context,"Yes response is run "+response,Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context,"Response Error"+error,Toast.LENGTH_LONG).show();
                    }
                })
                {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params = new HashMap<>();
                        params.put("pid",""+index.getId());
                        params.put("plus","yes");
                        params.put("cid",userid);
                        return params;
                    }
                };
                RequestQueue queue = Volley.newRequestQueue(context);
                queue.add(cartRequest);
//                StringRequest addtocart = new StringRequest(
//                        Request.Method.GET, url, new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//
//                    }
//                }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//
//                    }
//                }
//                );
            }
        });

        //decrement in cart
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(index.getQty() == 1){
                    Toast.makeText(context,"Value is already 1",Toast.LENGTH_SHORT).show();
                    return;
                }

                StringRequest dectocart = new StringRequest(
                        Request.Method.POST, ROOT_URL+"cart-dec-json.php", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        int qty = index.getQty()-1;
                        holder.qty.setText(""+qty);
                        Toast.makeText(context,"Cart Updated Successfully",Toast.LENGTH_SHORT).show();

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
                ){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params = new HashMap<>();
                        params.put("pid",""+index.getId());
                        params.put("dec","yes");
                        params.put("cart_id",""+index.getCartid());
                        params.put("cid",userid);
                        return params;
                    }
                };
                RequestQueue queue = Volley.newRequestQueue(context);
                queue.add(dectocart);
            }
        });


        //remove item from cart
        holder.dell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                AlertDialog.Builder alertBox = new AlertDialog.Builder(MainActivity.this,R.style.MyDialogTheme);
//                alertBox.setMessage("Internet Not Connected..\nPlease connect and start again");
//                alertBox.setCancelable(true);
//
//                alertBox.setPositiveButton(
//                        "Exit",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//
//                            }
//                        });
//                AlertDialog alert11 = alertBox.create();
//                alert11.show();
                StringRequest removefromcart = new StringRequest(
                        Request.Method.POST, ROOT_URL+"cart-dec-json.php", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        items.remove(position);
                        Toast.makeText(context,"Cart Item Removed Successfully",Toast.LENGTH_SHORT).show();
                        notifyDataSetChanged();
                        notifyItemChanged(position);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context,"ERROR : \n"+error,Toast.LENGTH_LONG).show();
                    }
                }
                ){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params = new HashMap<>();
                        params.put("pid",""+index.getId());
                        params.put("remove","yes");
                        params.put("cart_id",""+index.getCartid());
                        params.put("cid",userid);
                        return params;
                    }
                };
                RequestQueue queue = Volley.newRequestQueue(context);
                queue.add(removefromcart);
            }
        });

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent productdetial = new Intent(context, ProductDetail.class);
                productdetial.putExtra("product_id",index.getId());
                context.startActivity(productdetial);
                //  Toast.makeText(context,""+model.getId(),Toast.LENGTH_SHORT).show();
            }
        });
    }



    @Override
    public int getItemCount() {
        return items.size();
    }

    public  class CartHolder extends RecyclerView.ViewHolder{
        ImageView image,dell,minus,plus;
        TextView title, desc,price,qty;
        LinearLayout parentLayout;
        public CartHolder(View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.cart_image);
            title=itemView.findViewById(R.id.cart_item_title);
            desc=itemView.findViewById(R.id.cart_item_desc);
            price=itemView.findViewById(R.id.cart_item_price);
            dell = itemView.findViewById(R.id.dell);
            plus = itemView.findViewById(R.id.plus);
            qty = itemView.findViewById(R.id.qty);
            minus = itemView.findViewById(R.id.minus);
            parentLayout = itemView.findViewById(R.id.cartParentLayout);

        }
    }
}
