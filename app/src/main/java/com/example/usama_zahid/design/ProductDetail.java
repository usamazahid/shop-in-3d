package com.example.usama_zahid.design;


import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.usama_zahid.design.Adapters.SliderAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.example.usama_zahid.design.MainActivity.PRODUCTS_URL;
import static com.example.usama_zahid.design.MainActivity.ROOT_URL;


public class ProductDetail extends AppCompatActivity {

    NestedScrollView container;
    LinearLayout lyt_progress;
    LinearLayout btnPannel;
    int product_id;
    private final String PRODUCT_DETAIL = PRODUCTS_URL;
    SharedPreferences userPref;
    String userid;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        userPref = getSharedPreferences("userdata", MODE_PRIVATE);
        userid = userPref.getString("userid", null);

        lyt_progress  = findViewById(R.id.lyt_progress);
        lyt_progress.setVisibility(View.VISIBLE);
        lyt_progress.setAlpha(1.0f);
        container = findViewById(R.id.productdetial_layout);
        btnPannel = findViewById(R.id.btnpannel);
        btnPannel.setVisibility(View.GONE);
        container.setVisibility(View.GONE);

        Intent id = getIntent();
        product_id = id.getIntExtra("product_id",1);
        String url = PRODUCT_DETAIL+"?id="+product_id;
        productsRequest(url);


        Button addtocart = findViewById(R.id.addtocart);
        addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // String url1 = PRODUCT_DETAIL+"?id="+product_id;
                if (userid == null) {
                    finish();
                    Toast.makeText(getApplicationContext(),"Please Login First", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(getApplicationContext(), Login.class));
                    return;
                }
                addtoCart(ROOT_URL+"cart-json.php");
            }
        });

        Button ar = findViewById(R.id.placeobj);
        ar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ar_intent = new Intent(getApplicationContext(),ArActivity.class);
                ar_intent.putExtra("product_id",product_id);
                startActivity(ar_intent);
                Toast.makeText(getApplicationContext(),"Please Scan Flat Surface Area.. ",Toast.LENGTH_LONG).show();
            }
        });


    }

    public void productsRequest(String url){

        StringRequest productsRequest = new StringRequest(Request.Method.GET,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONArray productsJsonArray = new JSONArray(response);
                    for(int i=0;i<productsJsonArray.length();i++){
                        JSONObject product = productsJsonArray.getJSONObject(i);
                        int id = product.getInt("id");
                        String name = product.getString("name");
                        String des = product.getString("description");
                        double price = product.getDouble("price");
                        String image = product.getString("image");
                        String imageUrl = ROOT_URL+"images/products-images/"+image;

                        TextView nameTextView = findViewById(R.id.p_name);
                        TextView priceTextView = findViewById(R.id.p_price);
                        TextView descTextView = findViewById(R.id.p_desc);
                        ImageView imageView = findViewById(R.id.p_image);

                        nameTextView.setText(name);
                        priceTextView.setText(" PKR "+price);
                        descTextView.setText(des);
                        Glide.with(ProductDetail.this).load(imageUrl).into(imageView);

                        lyt_progress.setVisibility(View.GONE);
                        lyt_progress.setAlpha(1.0f);
                        container.setVisibility(View.VISIBLE);
                        btnPannel.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Toast.makeText(getApplicationContext(),"Yes response is run "+response,Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Network Error No Internet",Toast.LENGTH_LONG).show();
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(productsRequest);

    }


    public void addtoCart(String url){

        StringRequest cartRequest = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject cartdata = new JSONObject(response);
                    String result = (String) cartdata.get("message");
                    if(result.equals("success")){
                        showCustomDialog();
                    }
                    else if(result.equals("fail")){
                        Toast.makeText(getApplicationContext(),"Item not Added ",Toast.LENGTH_SHORT).show();
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"Faild to get response"+e,Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Network Error No Internet",Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("pid",""+product_id);
                params.put("plus","yes");
                params.put("cid",userid);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(cartRequest);

    }
    private void showCustomDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_cart);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        ((AppCompatButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

}
