package com.example.usama_zahid.design;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.usama_zahid.design.Adapters.CartAdapter;
import com.example.usama_zahid.design.Adapters.Product_List_Adapter;
import com.example.usama_zahid.design.Models.CartModel;
import com.example.usama_zahid.design.Models.Product_List_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.usama_zahid.design.MainActivity.ROOT_URL;

public class Cart extends AppCompatActivity {

    LinearLayout progress;
    LinearLayout container;



   public void detail(View v){
        startActivity(new Intent(this,ProductDetail.class));
    }

    ArrayList<CartModel> cartitems;
    RecyclerView cart_rec;
    SharedPreferences userPref;
    String userid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        userPref = getSharedPreferences("userdata", MODE_PRIVATE);
        userid = userPref.getString("userid", null);
        if (userid == null) {
            finish();
            Toast.makeText(getApplicationContext(),"Please Login First", Toast.LENGTH_LONG).show();
            startActivity(new Intent(this, Login.class));
            return;
        }

        progress = findViewById(R.id.lyt_progress);
        container = findViewById(R.id.cart_container);
       TextView checkout = findViewById(R.id.checkout);

       checkout.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               startActivity(new Intent(getApplicationContext(),Checkout.class));
           }
       });

        progress.setVisibility(View.VISIBLE);
        container.setVisibility(View.GONE);

        cart_rec =findViewById(R.id.cart_items_rec);
        cartitems= new ArrayList<CartModel>();
        cartRequest(ROOT_URL+"cart_rec_json.php?cid="+userid);



    }
    public void cartRequest(String url){
        StringRequest cartRequest = new StringRequest(Request.Method.GET,url, new Response.Listener<String>() {



            @Override
            public void onResponse(String response) {
                //Toast.makeText(getApplicationContext(),""+response,Toast.LENGTH_LONG).show();
                if(response.equals("null")){
                    Toast.makeText(getApplicationContext(),"Cart is Empty",Toast.LENGTH_LONG).show();
                            finish();
                            return;
                }
                try {
                    JSONArray cartJsonArray = new JSONArray(response);
                    for(int i=0;i<cartJsonArray.length();i++){
                        JSONObject cart = cartJsonArray.getJSONObject(i);
                            int id = cart.getInt("pro_id");
                            int qty = cart.getInt("qty");
                            int cartid = cart.getInt("cart_id");
                            String name = cart.getString("name");
                            String des = cart.getString("desc");
                            Double price = cart.getDouble("price");
                            String image = cart.getString("image");
                            String imageurl = ROOT_URL+"images/products-images/"+image;
                            cartitems.add(new CartModel(id,qty,cartid,imageurl,name,des.substring(0,25)+"...", price+" PKR"));
                       }

                    cart_rec.addItemDecoration(new DividerItemDecoration(cart_rec.getContext(), DividerItemDecoration.VERTICAL));
                    cart_rec.setLayoutManager(new LinearLayoutManager(Cart.this,LinearLayoutManager.VERTICAL,false));
                    CartAdapter cartAdapter = new CartAdapter(getApplicationContext(),cartitems,userid);
                    cartAdapter.notifyDataSetChanged();
                    cart_rec.setAdapter(cartAdapter);
                    progress.setVisibility(View.GONE);
                    container.setVisibility(View.VISIBLE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Cart Error : "+error.toString(),Toast.LENGTH_LONG).show();

            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(cartRequest);
    }
}
