package com.example.usama_zahid.design.Models;

public class CartModel {

    int id,qty,cartid;
    String product_image;
    String product_title,product_des,product_price;

    public CartModel(int id, int qty,int cartid, String product_image, String product_title, String product_des, String product_price) {
        this.id = id;
        this.qty = qty;
        this.product_image = product_image;
        this.product_title = product_title;
        this.product_des = product_des;
        this.product_price = product_price;
        this.cartid = cartid;
    }


    public int getCartid() {
        return cartid;
    }

    public void setCartid(int cartid) {
        this.cartid = cartid;
    }

    public int getId() {
        return id;
    }

    public int getQty() {
        return qty;
    }

    public String getProduct_image() {
        return product_image;
    }

    public String getProduct_title() {
        return product_title;
    }

    public String getProduct_des() {
        return product_des;
    }

    public String getProduct_price() {
        return product_price;
    }
}

