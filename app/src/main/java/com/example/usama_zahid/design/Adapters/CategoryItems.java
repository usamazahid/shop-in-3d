package com.example.usama_zahid.design.Adapters;

public class CategoryItems{

    int id;
    String title;
    String image;

  public CategoryItems(int id,String title,String image) {
      this.image = image;
      this.title = title;
      this.id = id;
  }

    public int getId() { return id; }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }


}
