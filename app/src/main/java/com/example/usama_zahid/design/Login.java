package com.example.usama_zahid.design;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.example.usama_zahid.design.MainActivity.ROOT_URL;

public class Login extends AppCompatActivity {

    SharedPreferences userPref;
    EditText email,password;
    TextView error,skip_login;
    Button login;
    ProgressBar progressBar;
    private final String CUSTOMERS_URL = ROOT_URL+"customers-json.php";

    public void register(View view){
        startActivity(new Intent(this,Register.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        userPref = getSharedPreferences("userdata", MODE_PRIVATE);
        String userid = userPref.getString("userid", null);
        if (userid != null) {
            finish();
            startActivity(new Intent(this, Profile.class));
            return;
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        skip_login = findViewById(R.id.skip_login);
        skip_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        error = findViewById(R.id.error);
        progressBar = findViewById(R.id.progress);

        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        login = findViewById(R.id.login_button);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(getApplicationContext(),"inside onclick",Toast.LENGTH_SHORT).show();
                login.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                userRequest(CUSTOMERS_URL);
            }
        });


    }

    void userRequest(String url){

        final String email = this.email.getText().toString().trim();
        final String password = this.password.getText().toString().trim();

        if(email.equals("") || password.equals("")){
            Toast.makeText(getApplicationContext(),"Please Fill all fields ",Toast.LENGTH_SHORT).show();
            error.setText("Please Fill all fields");
            error.setVisibility(View.VISIBLE);
            login.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            return;
        }


        StringRequest check = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try{
                    //Toast.makeText(getApplicationContext(),"your response "+response,Toast.LENGTH_LONG).show();
                    JSONObject jsondata = new JSONObject(response);
                    String result = (String) jsondata.get("found");

                    if(result.equals("1")){
                       String id = (String) jsondata.get("userid");

                        SharedPreferences sp = getSharedPreferences("userdata", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sp.edit();

                        editor.putString("userid", id);
                        editor.apply();

                        finish();
                        Intent gotohome = new Intent(getApplicationContext(),MainActivity.class);
                        gotohome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                       startActivity(gotohome);
                        Toast.makeText(getApplicationContext(),"Account Logged in Successfully ",Toast.LENGTH_SHORT).show();

                        //startActivity(new Intent(getApplicationContext(),Login.class));
                    }
                    else if(result.equals("0")){
                        //Toast.makeText(getApplicationContext(),"User Found not found ",Toast.LENGTH_SHORT).show();
                        error.setText("Username and Password is incorrect");
                        error.setVisibility(View.VISIBLE);
                        login.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);

                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"Faild to get response"+e,Toast.LENGTH_SHORT).show();
                    login.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getApplicationContext(),"Network Error No Internet",Toast.LENGTH_SHORT).show();
                error.setText("Network Error No Internet");
                error.setVisibility(View.VISIBLE);
                login.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("email",email);
                params.put("password",password);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(check);
    }
}
