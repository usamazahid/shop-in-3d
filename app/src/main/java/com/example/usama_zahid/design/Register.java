package com.example.usama_zahid.design;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import static com.example.usama_zahid.design.MainActivity.ROOT_URL;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;

public class Register extends AppCompatActivity {

    EditText name,email,password,address,phone;
    TextView error;
    ProgressBar progressBar;
    Button register;
    private final String REGISTER_URL = ROOT_URL+"registration-json.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        address = findViewById(R.id.address);
        phone = findViewById(R.id.phone);


        error = findViewById(R.id.error);
        progressBar = findViewById(R.id.progress);
        register = findViewById(R.id.register);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                register.setVisibility(View.GONE);
//                progressBar.setVisibility(View.VISIBLE);
                register(REGISTER_URL);
            }
        });


    }

    public void register(String url){

        final String name = this.name.getText().toString().trim();
        final String email = this.email.getText().toString().trim();
        final String password = this.password.getText().toString().trim();
        final String address = this.address.getText().toString().trim();
        final String phone = this.phone.getText().toString().trim();

        if (name.isEmpty()) {
            this.name.setError("Name Cant't Be Blank");
            this.name.requestFocus();
            return;
        }
        if (email.isEmpty()) {
            this.email.setError("Email Cant't Be Blank");
            this.email.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            this.password.setError("Password Cant't Be Blank");
            this.password.requestFocus();
            return;
        }
        if (this.password.length() < 6) {
            this.password.setError("Minimum length of Password is 6");
            this.password.requestFocus();
            return;
        }
        if (address.isEmpty()) {
            this.address.setError("Address Cant't Be Blank");
            this.address.requestFocus();
            return;
        }

        if (phone.isEmpty()) {
            this.phone.setError("phone Cant't Be Blank");
            this.phone.requestFocus();
            return;
        }
        if (this.phone.length() < 11) {
            this.phone.setError("Minimum length of Phone is 11");
            this.phone.requestFocus();
            return;
        }


        if( name.equals("") || email.equals("") || password.equals("") || address.equals("") || phone.equals("")){
            //Toast.makeText(getApplicationContext(),"Please Fill all fields ",Toast.LENGTH_SHORT).show();
            error.setVisibility(View.VISIBLE);
            error.setText("Please Fill all fields");
            progressBar.setVisibility(View.GONE);
            register.setVisibility(View.VISIBLE);
            return;
        }

        register.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        StringRequest create = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try{
                    //Toast.makeText(getApplicationContext(),"your response "+response,Toast.LENGTH_LONG).show();
                    JSONObject data = new JSONObject(response);
                    String result = (String) data.get("success");
                    if(result.equals("1")){
                        Toast.makeText(getApplicationContext(),"Account Created Succesfully",Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),"Faild to create Account"+e,Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                    register.setVisibility(View.VISIBLE);

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Network Error No Internet",Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("name",name);
                params.put("email",email);
                params.put("password",password);
                params.put("address",address);
                params.put("phone",phone);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(create);
    }
}
