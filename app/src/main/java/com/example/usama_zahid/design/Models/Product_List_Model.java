package com.example.usama_zahid.design.Models;

public class Product_List_Model {
    int id;
    String product_image;
    String product_title,product_des,product_price;


    public Product_List_Model(int id,String product_image, String product_title, String product_des, String product_price) {
        this.product_image = product_image;
        this.product_title = product_title;
        this.product_des = product_des;
        this.product_price = product_price;
        this.id = id;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getProduct_title() {
        return product_title;
    }

    public void setProduct_title(String product_title) {
        this.product_title = product_title;
    }

    public String getProduct_des() {
        return product_des;
    }

    public void setProduct_des(String product_des) {
        this.product_des = product_des;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }
}
