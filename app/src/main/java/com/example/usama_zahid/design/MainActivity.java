package com.example.usama_zahid.design;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.view.ViewPager;

import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.usama_zahid.design.Adapters.CategoryItems;
import com.example.usama_zahid.design.Adapters.Category_List_Adapter;
import com.example.usama_zahid.design.Adapters.Featured_Products_Adapter;
import com.example.usama_zahid.design.Adapters.SliderAdapter;
import com.example.usama_zahid.design.Models.Product_List_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String ROOT_URL = "https://shopin3d.000webhostapp.com/";
    public static final String CATEGORIES_URL = ROOT_URL+"categories-json.php";
    public static final String PRODUCTS_URL = ROOT_URL+"products-json.php";

    private SharedPreferences userPref ;
    private String userid;

    RecyclerView feature_products_rv;
    ArrayList<Product_List_Model> products = new ArrayList<Product_List_Model>();

    ArrayList<CategoryItems> categories = new ArrayList<CategoryItems>();
    ViewPager slider;
    SliderAdapter sliderAdapter;
    CircleIndicator sliderIndecator;

    private static final String TAG = MainActivity.class.getSimpleName();

    public void showCart(View v){ startActivity(new Intent(this, Cart.class)); }
    public void Login(View view){
        startActivity(new Intent(this, Profile.class));
    }
    public void products(View view){ startActivity(new Intent(this,Products.class)); }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!isNetworkConnected()){
            AlertDialog.Builder alertBox = new AlertDialog.Builder(MainActivity.this,R.style.MyDialogTheme);
            alertBox.setMessage("Internet Not Connected..\nPlease connect and start again");
            alertBox.setCancelable(true);

            alertBox.setPositiveButton(
                    "Exit",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    });

            AlertDialog alert11 = alertBox.create();
            alert11.show();
            return;
        }

        userPref = getSharedPreferences("userdata", MODE_PRIVATE);
        userid = userPref.getString("userid", null);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        productsRequest();
        categoriesRequest();

        //setting slider through SliderAdapter class in adapters folder
        slider = (ViewPager) findViewById(R.id.sliderViewPager);
        sliderAdapter = new SliderAdapter(this);
        sliderIndecator = findViewById(R.id.indicator);
        slider.setAdapter(sliderAdapter);
        sliderIndecator.setViewPager(slider);


        //app drawer code start
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);


        if (userid != null) {
            // get menu from navigationView
            Menu menu = navigationView.getMenu();
            // find MenuItem you want to change
            MenuItem nav_signin = menu.findItem(R.id.nav_signin);
            // set new title to the MenuItem
            nav_signin.setTitle("Profile");
        }

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
    }




    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {

        }
        else if (id == R.id.nav_cart) {
            startActivity(new Intent(this,Cart.class));
        }else if (id == R.id.nav_signin) {
            startActivity(new Intent(this, Login.class));
        } else if (id == R.id.nav_about) {
            startActivity(new Intent(this,About.class));
        } else if (id == R.id.nav_share) {
        } else if (id == R.id.nav_feedback) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    //Featured products request
    public void productsRequest(){
        StringRequest productsRequest = new StringRequest(Request.Method.GET,PRODUCTS_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    //example jason object
                    //JSONObject product = new JSONObject("{ \"id\": \"1\",\"name\": \"Alice\", \"description\": hahaha, \"price\": 1200 , \"image\": ammar.jpg }");
                    JSONArray productsJsonArray = new JSONArray(response);
                    for(int i=0;i<productsJsonArray.length();i++){
                        JSONObject product = productsJsonArray.getJSONObject(i);
                        int id = product.getInt("id");
                        String name = product.getString("name");
                        String des = product.getString("description");
                        double price = product.getDouble("price");
                        String image = product.getString("image");
                        String imageurl = ROOT_URL+"images/products-images/"+image;
                        products.add(new Product_List_Model(id,imageurl,name,des.substring(0,10)+"..", String.valueOf(price)+" PKR"));
                    }
                    feature_products_rv = findViewById(R.id.f_products_recyclerView);
                    feature_products_rv.setLayoutManager(new LinearLayoutManager(MainActivity.this,LinearLayoutManager.HORIZONTAL,false));
                    //feature_products_rv.addItemDecoration(new DividerItemDecoration(feature_products_rv.getContext(), DividerItemDecoration.VERTICAL));
                    feature_products_rv.setAdapter(new Featured_Products_Adapter(MainActivity.this,products));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Toast.makeText(getApplicationContext(),"Yes response is run "+response,Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                    Log.d("Categories Error ",error.toString());
                    Toast.makeText(getApplicationContext(),"Network Error No Internet",Toast.LENGTH_LONG).show();
            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(productsRequest);
    }

    //Categories request to web using volly
    public void categoriesRequest(){
        StringRequest categoriesRequest = new StringRequest(Request.Method.GET,CATEGORIES_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                RecyclerView categories_rv = findViewById(R.id.categories_rv);
                try {
                    JSONArray categoriesJsonArray = new JSONArray(response);
                    for(int i=0;i<categoriesJsonArray.length();i++){
                        JSONObject category = categoriesJsonArray.getJSONObject(i);
                        int id = category.getInt("id");
                        String name = category.getString("name");
                        String image = category.getString("image");
                        String imageurl = ROOT_URL+"images/categories-images/"+image;
                        //Toast.makeText(MainActivity.this,name,Toast.LENGTH_SHORT).show();
                        categories.add(new CategoryItems(id,name,imageurl));
                    }
                    categories_rv.setLayoutManager(new LinearLayoutManager(MainActivity.this,LinearLayoutManager.VERTICAL,false));
                    categories_rv.setHasFixedSize(true);
                    //categories_rv.addItemDecoration(new DividerItemDecoration(categories_rv.getContext(), DividerItemDecoration.VERTICAL));
                    categories_rv.setAdapter(new Category_List_Adapter(MainActivity.this,categories));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //Toast.makeText(getApplicationContext(),"Yes response is run "+response,Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                    Log.d("Categories Error ",error.toString());
                    Toast.makeText(getApplicationContext(),"Network Error No Internet",Toast.LENGTH_LONG).show();


            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(categoriesRequest);
    }

    //checking network connection
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }


}

