package com.example.usama_zahid.design;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.usama_zahid.design.Adapters.Product_List_Adapter;
import com.example.usama_zahid.design.Models.Product_List_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.usama_zahid.design.MainActivity.ROOT_URL;

public class Products extends AppCompatActivity {


    public static final String PRODUCTS_URL = ROOT_URL+"products-json.php";
    RecyclerView p_list;
    ArrayList<Product_List_Model> arraylist  = new ArrayList<Product_List_Model>();
    LinearLayout progress;

    SharedPreferences userPref;
    private  String userid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        userPref = getSharedPreferences("userdata", MODE_PRIVATE);
        userid = userPref.getString("userid", null);

        isNetworkConnected();
        progress = findViewById(R.id.lyt_progress);
        progress.setVisibility(View.VISIBLE);
        progress.setAlpha(1.0f);
        p_list  = findViewById(R.id.product_recyclerView);
        p_list.setVisibility(View.GONE);

        String url;
        Intent id = getIntent();
        int category_id = id.getIntExtra("category_id",0);
        if(category_id == 0){
             url = PRODUCTS_URL;
        }
        else{
             url = PRODUCTS_URL+"?category_id="+category_id;
        }
        productsRequest(url);

    }


    //Products request to web using volly
    public void productsRequest(String url){
        StringRequest productsRequest = new StringRequest(Request.Method.GET,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    //example json object
                    //JSONObject product = new JSONObject("{ \"id\": \"1\",\"name\": \"Alice\", \"description\": hahaha, \"price\": 1200 , \"image\": ammar.jpg }");
                    JSONArray productsJsonArray = new JSONArray(response);
                    for(int i=0;i<productsJsonArray.length();i++){
                        JSONObject product = productsJsonArray.getJSONObject(i);
                        int id = product.getInt("id");
                        String name = product.getString("name");
                        String des = product.getString("description");
                        double price = product.getDouble("price");
                        String image = product.getString("image");
                        String imageurl = ROOT_URL+"images/products-images/"+image;

                        arraylist.add(new Product_List_Model(id,imageurl,name,des.substring(0,25)+"...", String.valueOf(price)+" PKR"));
                    }

                    p_list.addItemDecoration(new DividerItemDecoration(p_list.getContext(), DividerItemDecoration.VERTICAL));
                    p_list.setLayoutManager(new LinearLayoutManager(Products.this,LinearLayoutManager.VERTICAL,false));
                    p_list.setAdapter(new Product_List_Adapter(Products.this,arraylist,userid));

                    progress.setVisibility(View.GONE);
                    p_list.setVisibility(View.VISIBLE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //Toast.makeText(getApplicationContext(),"Yes response is run "+response,Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"Products Error : "+error.toString(),Toast.LENGTH_LONG).show();

            }
        });
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(productsRequest);
    }

    //checking network connectivity
    private void isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm.getActiveNetworkInfo() == null){
            AlertDialog.Builder alertBox = new AlertDialog.Builder(Products.this,R.style.MyDialogTheme);
            alertBox.setMessage("Internet Not Connected..\nPlease connect and start again");
            //alertBox.setCancelable(true);

            alertBox.setPositiveButton(
                    "Exit",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    });

            AlertDialog alert11 = alertBox.create();
            alert11.show();
        }
    }

}
