package com.example.usama_zahid.design.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.usama_zahid.design.Login;
import com.example.usama_zahid.design.Models.Product_List_Model;
import com.example.usama_zahid.design.ProductDetail;
import com.example.usama_zahid.design.Products;
import com.example.usama_zahid.design.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.usama_zahid.design.MainActivity.ROOT_URL;

public class Product_List_Adapter extends RecyclerView.Adapter<Product_List_Adapter.ViewHolder> {

    private Context context;
    private ArrayList<Product_List_Model> arr;
    private String userid;

    public Product_List_Adapter(Products products, ArrayList<Product_List_Model> arr,String userid) {
        this.context = products;
        this.arr = arr;
        this.userid = userid;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v= inflater.inflate(R.layout.product_slice,parent,false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Product_List_Model model  = arr.get(position);

        holder.product_title.setText(model.getProduct_title());
        holder.product_des.setText(model.getProduct_des());
        holder.product_price.setText(model.getProduct_price());

        Glide.with(context).load(model.getProduct_image()).thumbnail(0.1f).into(holder.product_image);

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent productdetial = new Intent(context, ProductDetail.class);
                productdetial.putExtra("product_id",model.getId());
                context.startActivity(productdetial);
              //  Toast.makeText(context,""+model.getId(),Toast.LENGTH_SHORT).show();
            }
        });
        holder.addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (userid == null) {
                    Toast.makeText(context,"Please Login First", Toast.LENGTH_LONG).show();
                    context.startActivity(new Intent(context, Login.class));
                    return;
                }
                    StringRequest cartRequest = new StringRequest(Request.Method.POST,ROOT_URL+"cart-json.php", new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try{
                                JSONObject cartdata = new JSONObject(response);
                                String result = (String) cartdata.get("message");
                                if(result.equals("success")){
                                    showCustomDialog();
                                }
                                else if(result.equals("fail")){
                                    Toast.makeText(context,"Item not Added ",Toast.LENGTH_SHORT).show();
                                }
                            }
                            catch (JSONException e){
                                e.printStackTrace();
                                Toast.makeText(context,"Faild to get response"+e,Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(context,"Network Error No Internet",Toast.LENGTH_LONG).show();
                        }
                    })
                    {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String,String> params = new HashMap<>();
                            params.put("pid",""+model.getId());
                            params.put("plus","yes");
                            params.put("cid",userid);
                            return params;
                        }
                    };
                    RequestQueue queue = Volley.newRequestQueue(context);
                    queue.add(cartRequest);
                }
        });

    }

    @Override
    public int getItemCount() {
        return arr.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView product_image,addtocart;
        TextView product_title,product_des,product_price;
        LinearLayout parentLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            product_image = itemView.findViewById(R.id.product_image);
            product_title = itemView.findViewById(R.id.product_title);
            product_des = itemView.findViewById(R.id.product_des);
            product_price = itemView.findViewById(R.id.product_price);
            parentLayout = itemView.findViewById(R.id.product_slice);
            addtocart = itemView.findViewById(R.id.addtocart);


        }
    }

    private void showCustomDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_cart);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        ((AppCompatButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, ((AppCompatButton) v).getText().toString() + " Clicked", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
}
